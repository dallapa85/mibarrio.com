/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author dalla
 */
public class Conexion {
    
    String url = "jdbc:mysql://localhost:3306/mibarrio";
    String user= "root";
    String password="12345678";
    
    public Conexion (){
        
    }
    public Connection iniciarConexion() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        Connection  connection=DriverManager.getConnection(this.url,this.user,this.password);
        System.out.println("Connection is Successful to the database "+url);
        return connection;           
    }
}
