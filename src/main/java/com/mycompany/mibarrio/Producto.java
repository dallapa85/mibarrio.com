/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mibarrio;

import conexion.Conexion;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.SQLException;

/**
 *
 * @author dalla
 */
public class Producto {
    
    //Atributos
    private int id;
    private String nombre;
    private String tipo;
    private String fecha_ven;

    public Producto(int id, String nombre, String tipo, String fecha_ven) {
        super();
        this.id = id;
        this.nombre = nombre;
        this.tipo = tipo;
        this.fecha_ven = fecha_ven;
    }

    Producto() {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getFecha_ven() {
        return fecha_ven;
    }

    public void setFecha_ven(String fecha_ven) {
        this.fecha_ven = fecha_ven;
    }

   
    //CRUD
    
    public void guardar() throws ClassNotFoundException, SQLException{
        
        Conexion con = new Conexion(); // Instancia de la conexión
        String sql = "INSERT INTO productos(idproductos, nombre, tipo, fecha_ven) VALUES("+getId()+","+getNombre()+","+getTipo()+","+getFecha_ven()+");";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute (sql);
            
    }
    
    public void consultar() throws ClassNotFoundException, SQLException{
        
        Conexion con = new Conexion(); // Instancia de la conexión
        String sql = "SELECT idproductos,nombre,tipo,fecha_ven FROM productos WHERE nombre = " + getNombre() + ";";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();

        statement.execute (sql);
            
    }
    
    public void actualizar() throws ClassNotFoundException, SQLException{
        
        Conexion con = new Conexion(); // Instancia de la conexión
        //String sql = "INSERT INTO productos(id,nombre,tipo,fecha_ven) VALUES(" + getId() + "," + getNombre() + "," + getTipo() + "," + getFecha_ven() + ");";
        String sql = "UPDATE productos SET nombre ="+getNombre()+", tipo ="+getTipo()+", fecha_ven ="+getFecha_ven()+" WHERE idproductos ="+getId()+";";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();

        statement.execute (sql);
            
    }
    
    public void borrar() throws ClassNotFoundException, SQLException{
        
        Conexion con = new Conexion(); // Instancia de la conexión
        String sql = "DELETE FROM productos WHERE idproductos ="+getId()+";";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();

        statement.execute (sql);
            
    }
     @Override
    public String toString() {
        return "Producto{" + "id=" + id + ", nombre=" + nombre + ", tipo=" + tipo + ", fecha_ven=" + fecha_ven + '}';
    }
}
